package com.epam;

import java.io.*;

public class MyAutoCloseable implements AutoCloseable {

    public void close(){
        System.out.println("Closing MyAutoCloseable");
    }

    public void writeMessage(){
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("txt.txt"))){
            String msg = "Hello EPAM!";
            writer.write(msg);
            System.out.println("Message written to file successfuly!");
        }catch(Exception exception){
            System.out.println("Something went wrong!");
        }
    }

    public static void main(String[] args) {
        try(MyAutoCloseable myAutoCloseable = new MyAutoCloseable()){
            myAutoCloseable.writeMessage();
        }
    }
}
