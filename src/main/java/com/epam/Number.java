package com.epam;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * Class Number created to do couple of things with numbers.
 *
 * @author Rostyk Fedchuk
 * @version 1.0
 */
public class Number {
    /**
     * Represents the scanner for all variables in the class.
     */
    private static final Scanner SCANNER;
    /**
     * Represents the start of range.
     */
    private static int start;
    /**
     * Represents the end of range.
     */
    private static int end;
    /**
     * Contains a set of fibonacci numbers,
     * starting with chosen F1 and F2 numbers.
     */
    private Set<Integer> set = new HashSet<Integer>();

    /**
     * Represents hundred percent.
     */
    static final int HUNDRED_PERCENT = 100;

    /**
     * Static block is used for enter the interval.
     * It checks the accuracy of the input.
     */
    static {
        SCANNER = new Scanner(System.in, "UTF-8");
        boolean isCorrect = true;
        while (isCorrect) {
            System.out.println("Enter the interval (for example: [1;100]):");
            start = SCANNER.nextInt();
            end = SCANNER.nextInt();
            if (end < start) {
                try {
                    throw new OutOfRangeException();
                } catch (OutOfRangeException e) {
                    e.showMessage();
                }
            } else {
                break;
            }
        }
    }

    /**
     * Prints Odd and even numbers from the range.
     */
    public void printOddEven() {
        System.out.print("Odd numbers: ");
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.print("\nEven numbers: ");
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * Prints the sum of Odd and even numbers from the range.
     */
    public void printSum() {
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = start; i < end; i++) {
            if (i % 2 != 0) {
                sumOdd += i;
            } else {
                sumEven += i;
            }
        }
        System.out.println("Sum of odd numbers from "
                + start + " to " + end + " = " + sumOdd);
        System.out.println("Sum of even numbers from "
                + start + " to " + end + " = " + sumEven);
    }

    /**
     * Builds fibonacci number starting with
     * chosen F1(max odd number) and F2(max even number) numbers.
     * This method implements recursion.
     *
     * @param n Index of fibonacci numbers.
     * @return An int representing n-th fibonacci number.
     */
    public int builtFibonacci(final int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1) {
            return maxOdd();
        } else if (n == 2) {
            return maxEven();
        } else {
            return builtFibonacci(n - 1) + builtFibonacci(n - 2);
        }
    }

    /**
     * Fills the set with fibonacci numbers and prints it.
     */
    public void builtFibonacciSet() {
        System.out.println("Enter count of fibonacci numbers:");
        int countNumber = SCANNER.nextInt();
        System.out.print("Fibonacci numbers: ");
        for (int i = 1; i <= countNumber; i++) {
            set.add(builtFibonacci(i));
            System.out.print(builtFibonacci(i) + " ");
        }
    }

    /**
     * Prints the percentage of odd and even fibonacci numbers.
     */
    public void getPercentage() {
        int countOdd = 0;
        int countEven = 0;
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            if (it.next() % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }
        int oddPercentage = countOdd * HUNDRED_PERCENT / set.size();
        int evenPercentage = countEven * HUNDRED_PERCENT / set.size();
        System.out.println("\nOdd number percentage = " + oddPercentage
                + "\nEven number percentage = " + evenPercentage);
    }

    /**
     * Gets maximum odd number from the range.
     *
     * @return An int representing maximum odd number from the range.
     */
    public int maxOdd() {
        int maxOdd = 0;
        for (int i = start; i < end; i++) {
            if (i % 2 != 0 && maxOdd < i) {
                maxOdd = i;
            }
        }
        return maxOdd;
    }

    /**
     * Gets maximum even number from the range.
     *
     * @return An int representing maximum even number from the range.
     */
    public int maxEven() {
        int maxEven = 0;
        for (int i = start; i < end; i++) {
            if (i % 2 == 0 && maxEven < i) {
                maxEven = i;
            }
        }
        return maxEven;
    }
}
