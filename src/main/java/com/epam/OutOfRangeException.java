package com.epam;

public class OutOfRangeException extends Throwable{
    public void showMessage(){
        System.out.println("Incorrect range! "
                + "End index cannot be less than start index.");
    }
}
