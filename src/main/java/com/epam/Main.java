package com.epam;

/**
 * Class Main created to implements all methods from Number class.
 *
 * @author Rostyk Fedchuk
 * @version 1.0
 */
public final class Main {
    /**
     * Initializes an Number instance.
     * Implements all methods of Number`s instance.
     *
     * @param args Command line arguments.
     */
    public static void main(final String[] args) {
        Number obj = new Number();
        obj.printOddEven();
        obj.printSum();
        obj.builtFibonacciSet();
        obj.getPercentage();
    }

    /**
     * Constructor of Main class.
     * It`s private because of no existing instance.
     */
    private Main() {

    }
}
